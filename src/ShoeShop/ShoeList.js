import React, { Component } from 'react'
import ShoeItem from './ShoeItem'

export default class ShoeList extends Component {
    renderShoeList =()=>{
        return this.props.list.map((item)=>
        {
            return <ShoeItem item = {item}/>
        })
    }
    render() {
    return (
      <div className='row'>
        {this.renderShoeList()}
      </div>
    )
  }
}
