import React, { Component } from "react";
import { connect } from "react-redux";

class ShoeCart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price*item.quantitybuy}</td>
          <td>
            <img src={item.image} style={{ width: 50 }} alt="" />
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => this.props.handleGiam(item.id)}
            >
              -
            </button>
            {item.quantitybuy}
            <button
              className="btn btn-success"
              onClick={() => this.props.handleTang(item.id)}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => this.props.handleXoa(item.id)}
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Img</th>
            <th>Qty</th>
            <th>Thao tác</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleGiam: (shoe) => {
      let action = { type: "GIAM", payload: shoe };
      dispatch(action);
    },
    handleTang: (shoe) => {
      dispatch({ type: "TANG", payload: shoe });
    },
    handleXoa: (shoe) => {
      dispatch({ type: "XOA", payload: shoe });
    },
  };
};
export default connect(null,mapDispatchToProps)(ShoeCart)