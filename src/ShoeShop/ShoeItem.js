import React, { Component } from "react";
import { connect } from "react-redux";

class ShoeItem extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="col-3 py-4">
        <div className="d-flex flex-column">
          <button
            type="button"
            data-toggle="modal"
            data-target="#exampleModalCenter"
          >
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text">{price}</p>
                <img
                  className="card-img-top"
                  src={image}
                  alt="Card image cap"
                />
              </div>
            </div>
          </button>
          <button
            className="btn btn-primary"
            onClick={() => this.props.handlePushToCart(this.props.item)}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlePushToCart: (shoe) => {
      let action = { type: "THEM", payload: shoe };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ShoeItem);
