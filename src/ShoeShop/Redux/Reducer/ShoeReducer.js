import { dataShoe } from "../../DataShoe";

let initialValue = {
  shoeList: dataShoe,
  cartList: [],
};

export const shoeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "THEM": {
      let cloneCart = [...state.cartList];
      let viTri = cloneCart.findIndex((item) => action.payload.id == item.id);

      if (viTri == -1) {
        let newShoe = { ...action.payload, quantitybuy: 1 };
        cloneCart.push(newShoe);
        return { ...state, cartList: cloneCart };
      } else {
        cloneCart[viTri].quantitybuy++;
        return { ...state, cartList: cloneCart };
      }
    }
    case "TANG": {
      let viTri = state.cartList.findIndex((item) => item.id == action.payload);
      let cloneCart = [...state.cartList];
      cloneCart[viTri].quantitybuy++;
      return { ...state, cartList: cloneCart };
    }

    case "GIAM": {
      let viTri = state.cartList.findIndex((item) => item.id == action.payload);
      let cloneCart = [...state.cartList];
      if (cloneCart[viTri].quantitybuy > 1) {
        cloneCart[viTri].quantitybuy--;
      } else {
        cloneCart.splice(viTri, 1);
      }

      return { ...state, cartList: cloneCart };
    }
    case "XOA": {
      let viTri = state.cartList.findIndex((item) => item.id == action.payload);
      let newCart = [...state.cartList];
      newCart.splice(viTri, 1);
      return { ...state, cartList: newCart };
    }
    default: {
      return state;
    }
  }
};
