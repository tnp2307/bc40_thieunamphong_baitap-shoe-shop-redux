import { combineReducers } from "redux";
import { shoeReducer } from "./ShoeReducer";

export const rootReducer = combineReducers({shoeReducer})