import React, { Component } from 'react'
import { connect } from 'react-redux'

import  ShoeCart from './ShoeCart';
import ShoeList from './ShoeList';

class ShoeShop extends Component {
  render() {
    console.log(this.props.cart);
    return (
      <div className='container'>
        <ShoeCart cart = {this.props.cart} />
       

        <ShoeList list ={this.props.list}/>
    
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
    list: state.shoeReducer.shoeList,
    cart: state.shoeReducer.cartList,
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(ShoeShop)